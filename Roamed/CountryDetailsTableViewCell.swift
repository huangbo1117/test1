//
//  CountryDetailsTableViewCell.swift
//  Roamed
//
//  Created by QL on 27/7/16.
//  Copyright © 2016 SIMPSY LLP. All rights reserved.
//

import UIKit

class CountryDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var countryImage: UIImageView!

    @IBOutlet weak var countryText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
