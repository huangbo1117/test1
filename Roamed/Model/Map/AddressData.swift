//
//  AddressData.swift
//  TravPholer
//
//  Created by BoHuang on 12/8/16.
//  Copyright © 2016 BoHuang. All rights reserved.
//

import Foundation
import GoogleMaps
class AddressData1:BaseModelSwift{
    var address:String?
    var city:String?
    var country:String?
    var pos:CLLocationCoordinate2D?
    
    var city_hubos:[String]?
    
    var city_hubo1:String?
    var city_hubo2:String?
    
    var type = -1;
    var placeId:String?
    
    
    
    init(dictionary:[String:Any]){
        super.init()
        BaseModelSwift.parseResponse(targetClass: self, dict: dictionary)
    }
    
    func setAddressData(){
        guard let country = country,let city_hubos = city_hubos else{
            return;
        }
        if !country.isEmpty && city_hubos.count>0{
            let city = city_hubos[0]
            address = city + "," + country
        }
    }
    
    required init(){
        
    }
}
