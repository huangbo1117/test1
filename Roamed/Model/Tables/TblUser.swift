//
//  TblUser.swift
//  TravPholer
//
//  Created by BoHuang on 11/26/16.
//  Copyright © 2016 BoHuang. All rights reserved.
//

import Foundation

class TblUser:BaseModelSwift{
    var tu_id:String?
    var tu_username:String?
    var tu_password:String?
    var tu_type:String?
    var tu_apnid:String?
    var tu_gcmid:String?
    var tu_pic:String?
    var create_datetime:String?
    var modify_datetime:String?
    var tu_role:String?
    var tu_email:String?
    var tu_firstname:String?
    var tu_lastname:String?
    var tu_likesrecv:String?
    var tbr_id:String?
    var tba_list:String?
    
    var tu_pre_distance:String?
    var tu_pre_currency:String?
    var tu_pre_location_name:String?
    var tu_pre_location_lat:String?
    var tu_pre_location_lng:String?
    var tu_pre_page_explore:String?
    var tu_pre_suggestion:String?
    var tu_pre_public:String?
    var tu_noti_news:String?
    var tu_pre_time:String?
}
