//
//  CountryTableViewController.swift
//  Roamed
//
//  Created by QL on 27/7/16.
//  Copyright © 2016 SIMPSY LLP. All rights reserved.
//

import UIKit
import CoreData

class CountryTableViewController: UITableViewController {
    var countryarray = [String]()
    var isoarray = [String]()
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        loadCountry()
        getCountryData()
    }
    
    func loadCountry(){
        
        
        let appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate //set AppDelegate
        
        let context: NSManagedObjectContext = appDel.managedObjectContext //handler to access the database via manageObjectContext
        
        
        let url = URL(string: "http://roamed.co/api/get_country_iso/?key=abc123&secret=123abc")
        let task = URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            if let urlContent = data{
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
                        let totalCount:Int = (jsonResult as AnyObject).count
                        print(totalCount)
                        
                        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Country")
                        fetchRequest.returnsObjectsAsFaults = false
                        
                        do
                        {
                            let results = try context.fetch(fetchRequest)
                            for managedObject in results
                            {
                                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                                context.delete(managedObjectData)
                            }
                        } catch let error as NSError {
                            print("Detele all data error : \(error) \(error.userInfo)")
                        }
                        
                        
                        
                        for i in 1...totalCount{
                            if let jobj = jsonResult["\(i)"] as? [String:String]{
                                let country = jobj["country"]
                                let iso = jobj["iso"]
                                let newCountry = NSEntityDescription.insertNewObject(forEntityName: "Country", into: context)
                                newCountry
                                    .setValue(country, forKey: "country")
                                newCountry.setValue(iso, forKey: "iso")
                                do{ try context.save() // save to database
                                    print("inserted")
                                }catch{
                                    
                                    print("There was a problem")
                                    
                                }
                                
                                
                                
                            }
                        }
                    }
                    
                    
                    
                }catch{
                    
                }
                
                
            }
            
        })
        task.resume()
        
        
    }
    func getCountryData(){
        
        let appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate //set AppDelegate
        
        let context: NSManagedObjectContext = appDel.managedObjectContext //handler to access the database via manageObjectContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Country")
        
        request.returnsObjectsAsFaults = false //by setting to false will see the actual result rather then how many data in the database
        
        do{
            let results = try context.fetch(request)
            if results.count > 0{
                
                for result in results as! [NSManagedObject]{ //loop for getting result
                    
                    if let countryGet = result.value(forKey: "country") as? String{
                        self.countryarray.append(countryGet)
                        
                        if let isoGet = result.value(forKey: "iso") as? String{
                            self.isoarray.append(isoGet)
                            
                        }
                        
                    }
                    
                    
                }
            }
            
        }catch{
            
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return countryarray.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCountryCell = tableView.dequeueReusableCell(withIdentifier: "countrycell", for: indexPath) as! CountryDetailsTableViewCell
        myCountryCell.countryImage.layer.cornerRadius =  myCountryCell.countryImage.frame.size.width / 2
        // myCell.countryImage.layer.borderColor = UIColor.whiteColor().CGColor
        //myCell.countryImage.layer.borderWidth = 1
        myCountryCell.countryImage.clipsToBounds = true
        
        myCountryCell.countryImage.image = UIImage(named: "\(isoarray[(indexPath as NSIndexPath).row])")
        myCountryCell.countryText.text = countryarray[(indexPath as NSIndexPath).row]
        
        return myCountryCell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
