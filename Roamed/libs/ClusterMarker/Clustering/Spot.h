//
//  Spot.h
//  Google Maps iOS Example
//
//  Created by Colin Edwards on 2/1/14.
//
//

#import <Foundation/Foundation.h>

@import CoreLocation;
#import "GClusterItem.h"

@class TblPhotos;

@interface Spot : NSObject <GClusterItem>

@property (nonatomic) CLLocationCoordinate2D location;

@property (nonatomic, strong) GMSMarker *marker;

@property (nonatomic, copy) NSString *countryId;

@property (nonatomic, copy) NSString *tp_id;


-(instancetype)initWithPhoto:(TblPhotos*) data;
@end
