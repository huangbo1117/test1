//
//  Spot.m
//  Google Maps iOS Example
//
//  Created by Colin Edwards on 2/1/14.
//
//

#import "Spot.h"
#import "Roamed-Swift.h"

@implementation Spot

- (CLLocationCoordinate2D)position {
    return self.location;
}
-(instancetype)initWithPhoto:(TblPhotos*) data{
    self = [super init];
    if(self){
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.title = @"";
//        marker.position = CLLocationCoordinate2DMake(
//                                                     [data.tp_lat floatValue] ,
//                                                     [data.tp_lon floatValue]);
        
        
        _location = marker.position;
        _marker = marker;
        
//        _countryId = data.tp_countryid;
//        _tp_id = data.tp_id;
        
        
    }
    return self;
}
@end
