#import <Foundation/Foundation.h>
#import "GClusterItem.h"

@protocol GClusterAlgorithm <NSObject>

- (void)addCluster:(NSMutableArray*) item;
- (void)addItem:(id <GClusterItem>) item;
- (void)removeItems;
- (void)removeItemsNotInRectangle:(CGRect)rect;

- (NSSet*)getClusters:(float)zoom;
- (NSSet*)getClustersForCountry:(float)zoom;

- (void)setCountryData;
@end
