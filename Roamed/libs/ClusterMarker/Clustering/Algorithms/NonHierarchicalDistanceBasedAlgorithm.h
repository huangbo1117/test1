#import <Foundation/Foundation.h>
#import "GClusterAlgorithm.h"
#import "GQTPointQuadTree.h"

@interface NonHierarchicalDistanceBasedAlgorithm : NSObject<GClusterAlgorithm> 

@property (nonatomic, strong) NSMutableArray *items;

@property (nonatomic, strong) NSMutableDictionary *countries;
@property (nonatomic, strong) NSMutableDictionary *quadTrees;

- (id)initWithMaxDistanceAtZoom:(NSInteger)maxDistanceAtZoom;

@end
