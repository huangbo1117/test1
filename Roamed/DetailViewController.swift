//
//  DetailViewController.swift
//  Roamed
//
//  Created by QL on 25/7/16.
//  Copyright © 2016 SIMPSY LLP. All rights reserved.
//

import UIKit
import CoreData

class DetailViewController: UIViewController {
    var countryResult:String =  ""
    var dateResult:String =  ""
    var imageResult:String = ""
    var countrycodeResult:String = ""
    var typeResult:String = ""
    
    @IBOutlet weak var countryText: UIButton!
    
    @IBAction func countryButton(_ sender: AnyObject) {
        
        
    }
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var dates: UILabel!
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var overseasNumber: UITextField!
    @IBAction func updateButton(_ sender: AnyObject) {
    }
    var detailtId = String()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        let appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate //set AppDelegate
        
        let context: NSManagedObjectContext = appDel.managedObjectContext //handler to access the database via manageObjectContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Past_Purchased")
        
        
        request.predicate = NSPredicate(format: "id = %@", "\(self.detailtId)") //search the database let username = tommy, %@ represent string
        
        request.returnsObjectsAsFaults = false
        
        do{
            let results = try context.fetch(request)
            
            if results.count > 0{
                
                for result in results as! [NSManagedObject]{ //loop for getting result
                    
                    if let ended = result.value(forKey: "ended") as? String{ //set username as string
                        self.dateResult = "Ended on \(ended)"
                        
                        //self.dates.text = "Ended on \(ended)"
                        
                        if let country = result.value(forKey: "country") as? String{
                            //self.overseasNumber.attributedPlaceholder = NSAttributedString(string:"Insert\(country) Number")
                            //self.countryText.setTitle("\(country) >", forState: UIControlState.Normal)
                            self.countryResult = country
                            if let imageGet = result.value(forKey: "iso") as? String{
                                //self.countryImage.layer.cornerRadius =  self.countryImage.frame.size.width / 2
                                self.imageResult = imageGet
                                
                                //self.countryImage.clipsToBounds = true
                                //self.countryImage.image = UIImage(named: "\(imageGet)")
                                //self.overseasNumber.userInteractionEnabled = false
                                let url = URL(string: "http://roamed.co/api/get_country_code/?key=abc123&secret=123abc&iso=\(imageGet)")
                                
                                let task = URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                                    
                                    if let urlContent = data{
                                        do {
                                            if let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
                                                let getCoutryCode = jsonResult["country_code"] as! String
                                                
                                                self.typeResult = "Past"
                                                DispatchQueue.main.async{
                                                    self.countryCode.text = "+\(getCoutryCode)"
                                                    self.overseasNumber.isUserInteractionEnabled = false
                                                }
                                            }
                                            
                                            
                                            
                                            
                                        }catch{
                                            
                                        }
                                    }
                                    
                                })
                                task.resume()
                                
                                
                                
                                
                            }
                        }
                        
                        
                    }
                }
            }else{
                
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Present_Purchase")
                
                
                
                request.predicate = NSPredicate(format: "id = %@", "\(self.detailtId)") //search the database let username = tommy, %@ represent string
                
                request.returnsObjectsAsFaults = false
                do{
                    
                    let results = try context.fetch(request)
                    if results.count > 0{
                        
                        for result in results as! [NSManagedObject]{ //loo
                            if let ended = result.value(forKey: "ended") as? String{ //set username as string
                                //self.dates.text = " \(ended) days left"
                                self.dateResult = " \(ended) days left"
                                if let country = result.value(forKey: "country") as? String{
                                    //self.overseasNumber.attributedPlaceholder = NSAttributedString(string:"Insert\(country) Number")
                                    self.countryResult = country
                                    //self.countryText.setTitle("\(country) >", forState: UIControlState.Normal)
                                    if let imageGet = result.value(forKey: "iso") as? String{
                                        // self.countryImage.layer.cornerRadius =  self.countryImage.frame.size.width / 2
                                        //self.countryImage.clipsToBounds = true
                                        //self.countryImage.image = UIImage(named: "\(imageGet)")
                                        self.imageResult = imageGet
                                        let url = URL(string: "http://roamed.co/api/get_country_code/?key=abc123&secret=123abc&iso=\(imageGet)")
                                        
                                        let task = URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                                            if let urlContent = data{
                                                do {
                                                    if let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
                                                        let getCoutryCode = jsonResult["country_code"] as! String
                                                        
                                                        
                                                        self.typeResult = "Present"
                                                        DispatchQueue.main.async{
                                                            self.countryCode.text = "+\(getCoutryCode)"
                                                        }
                                                        
                                                    }
                                                    
                                                    
                                                }catch{
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                        })
                                        task.resume()
                                        
                                    }
                                    
                                }
                                
                            }
                        }
                        
                        
                    }
                    
                    
                    
                }catch{
                    
                }
                
                
                
            }
            
            
        }catch{
            
            
        }
        
        DispatchQueue.main.async(execute: {
            self.dates.text = self.dateResult
            self.overseasNumber.attributedPlaceholder = NSAttributedString(string:"Insert\(self.countryResult) Number")
            self.countryText.setTitle("\(self.countryResult) >", for: UIControlState())
            self.countryImage.layer.cornerRadius =  self.countryImage.frame.size.width / 2
            self.countryImage.clipsToBounds = true
            self.countryImage.image = UIImage(named: "\(self.imageResult)")
            //self.countryCode.text = "+\(self.countrycodeResult)"
            
            
            
        })
        
        
        
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) { //close the keyboard when touches anywhere
        
        self.view.endEditing(true)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
