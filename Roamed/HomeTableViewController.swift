//
//  HomeTableViewController.swift
//  Roamed
//
//  Created by QL on 18/7/16.
//  Copyright © 2016 SIMPSY LLP. All rights reserved.
//

import UIKit
import CoreData

class HomeTableViewController: UITableViewController {
    
    var pastCountryPrint =  [String]()
    var pastIsoPrint = [String]()
    var pastDays = [String]()
    var pastMinutes = [String]()
    var pastId = [String]()
    
    
    
    var refresher: UIRefreshControl!
    var navBar:UINavigationBar=UINavigationBar()
    
    func refresh(){
        
        print("Refreshed")
        
        //update code
        
        self.refresher.endRefreshing() //remove refresh
        
    }
    
    let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        getPurchased()
        getPresentCoreData()
        getPastCoreData()
        
        refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refresher.addTarget(self, action: #selector(HomeTableViewController.refresh), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refresher)
        refresh()
        
        
    }
    
    
    
    
    
    func getPurchased()  {
        
        let appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate //set AppDelegate
        
        let context: NSManagedObjectContext = appDel.managedObjectContext //handler to access the database via manageObjectContext
        
        let url = URL(string: "http://roamed.co/api/get_purchased/?key=abc123&secret=123abc&userid=12&phone=6592997764")
        let task = URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            if let urlContent = data{
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
                        let totalCount = (jsonResult as AnyObject).count
                        
                        if let presentPurchase = jsonResult["present_purchase"] as? [String:Any]{
                            
                            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Present_Purchase")
                            fetchRequest.returnsObjectsAsFaults = false
                            do
                            {
                                let results = try context.fetch(fetchRequest)
                                for managedObject in results
                                {
                                    let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                                    context.delete(managedObjectData)
                                }
                            } catch let error as NSError {
                                print("Detele all data error : \(error) \(error.userInfo)")
                            }
                            
                            let countPresent:Int = presentPurchase.count
                            
                            for i in 1...countPresent {
                                if (presentPurchase["\(i)"]) != nil{
                                    if let presentDetail = presentPurchase["\(i)"] as? [String:String] {
                                        var presentId = presentDetail["id"]
                                        let presentCountry = presentDetail["country"]
                                        let presentIso = presentDetail["country_iso"]
                                        let presentEnded = presentDetail["days_left"]
                                        let presentMinutes = presentDetail["minutes_left"]
                                        
                                        
                                        let newUser = NSEntityDescription.insertNewObject(forEntityName: "Present_Purchase", into: context)
                                        
                                        newUser.setValue(presentCountry, forKey: "country")
                                        newUser.setValue(presentId, forKey: "id")
                                        newUser.setValue(presentEnded, forKey: "ended")
                                        newUser.setValue(presentIso, forKey: "iso")
                                        newUser.setValue(presentMinutes, forKey: "minutes_left")
                                        
                                        do{ try context.save() // save to database
                                            print("inserted")
                                        }catch{
                                            
                                            print("There was a problem")
                                            
                                        }
                                    }
                                    
                                    
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        
                        if let pastPurchase = jsonResult["past_purchase"] {
                            
                            
                            //delete coredata if there is result, so that to update the core data
                            
//                            let fetchRequest : NSFetchRequest<NSFetchRequestResult> = pastPurchase.fetchRequest()
//                            fetchRequest.returnsObjectsAsFaults = false
//                            
//                            do
//                            {
//                                let results = try context.fetch(fetchRequest)
//                                for managedObject in results
//                                {
//                                    let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
//                                    context.delete(managedObjectData)
//                                }
//                            } catch let error as NSError {
//                                print("Detele all data error : \(error) \(error.userInfo)")
//                            }
//                            
//                            
//                            let countPast = pastPurchase.count
//                            
//                            
//                            
//                            for i in 1...countPast {
//                                
//                                
//                                if (pastPurchase["\(i)"]) != nil{
//                                    let pastDetail = pastPurchase["\(i)"]!!
//                                    var pastId:String = pastDetail["id"] as! String
//                                    let pastCountry:String = pastDetail["country"] as! String
//                                    let pastIso:String = pastDetail["country_iso"] as! String
//                                    let pastEnded:String = pastDetail["ended"] as! String
//                                    let pastMinutes = pastDetail["minutes_left"] as! String
//                                    
//                                    
//                                    
//                                    let newUser = NSEntityDescription.insertNewObject(forEntityName: "Past_Purchased", into: context)
//                                    
//                                    newUser.setValue(pastCountry, forKey: "country")
//                                    newUser.setValue(pastId, forKey: "id")
//                                    newUser.setValue(pastEnded, forKey: "ended")
//                                    newUser.setValue(pastIso, forKey: "iso")
//                                    newUser.setValue(pastMinutes, forKey: "minutes_left")
//                                    
//                                    do{ try context.save() // save to database
//                                        print("inserted")
//                                    }catch{
//                                        
//                                        print("There was a problem")
//                                        
//                                    }
//                                    
//                                    
//                                    
//                                    //var dicDetails  = ["id": pastId ]
//                                    //self.past.append(dicDetails)
//                                    
//                                    
//                                    
//                                    
//                                    
//                                }
//                                
//                            }
                            
                            
                            
                            
                            // print(pastPurchase["1"]!!["id"])
                            
                        }
//                        if let presentPurchase = jsonResult["present_purchase"]!{
//                            
//                            let countPresent = presentPurchase.count
//                            
//                            //  print(presentPurchase)
//                            
//                            
//                        }
                    }
                    
                    
                    
                    
                    
                }catch{
                    
                    print("JSON Serialization failed")
                    
                }
                
                
                
            }
            
            
            
        })
        task.resume()
        
        
        
    }
    
    func getPresentCoreData(){
        
        
        let appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate //set AppDelegate
        
        let context: NSManagedObjectContext = appDel.managedObjectContext //handler to access the database via manageObjectContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Present_Purchase")
        
        request.returnsObjectsAsFaults = false //by setting to false will see the actual result rather then how many data in the database
        
        do{
            let results = try context.fetch(request)
            
            if results.count > 0{
                
                for result in results as! [NSManagedObject]{ //loop for getting result
                    
                    if let countryGet = result.value(forKey: "country") as? String{ //set username as string
                        
                        self.pastCountryPrint.append(countryGet)
                        if let isoGet = result.value(forKey: "iso") as? String{
                            self.pastIsoPrint.append(isoGet)
                            
                            if let minutesGet = result.value(forKey: "minutes_left") as? String{
                                self.pastMinutes.append(minutesGet)
                                if let daysGet = result.value(forKey: "ended") as? String{
                                    self.pastDays.append(daysGet)
                                    
                                    
                                    if let idGet = result.value(forKey: "id") as? String{
                                        
                                        self.pastId.append(idGet)
                                    }
                                }
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    
                    
                    
                }
                
            }
            
        }catch{
            
            print("Fetch Failed")
            
        }
        
        
        
    }
    
    func getPastCoreData(){
        
        
        let appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate //set AppDelegate
        
        let context: NSManagedObjectContext = appDel.managedObjectContext //handler to access the database via manageObjectContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Past_Purchased")
        
        request.returnsObjectsAsFaults = false //by setting to false will see the actual result rather then how many data in the database
        
        do{
            let results = try context.fetch(request)
            
            if results.count > 0{
                
                for result in results as! [NSManagedObject]{ //loop for getting result
                    
                    if let countryGet = result.value(forKey: "country") as? String{ //set username as string
                        
                        self.pastCountryPrint.append(countryGet)
                        if let isoGet = result.value(forKey: "iso") as? String{
                            self.pastIsoPrint.append(isoGet)
                            
                            if let minutesGet = result.value(forKey: "minutes_left") as? String{
                                self.pastMinutes.append(minutesGet)
                                if let daysGet = result.value(forKey: "ended") as? String{
                                    self.pastDays.append("expired")
                                    
                                    
                                    if let idGet = result.value(forKey: "id") as? String{
                                        
                                        self.pastId.append(idGet)
                                    }
                                }
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    
                    
                    
                }
                
            }
            
        }catch{
            
            print("Fetch Failed")
            
        }
        
        
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        let tableRow = pastIsoPrint.count + 1
        
        return tableRow
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "purchaseDetails"{
            
            
            let destination = segue.destination as? DetailViewController
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                
                let selectedRow = (indexPath as NSIndexPath).row - 1 //due to the first one being header
                destination!.detailtId = pastId[selectedRow]
            }
            
        }
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell
        
        // Configure the cell...
        
        if (indexPath as NSIndexPath).row == 0{
            myCell.selectionStyle = UITableViewCellSelectionStyle.none
            myCell.isUserInteractionEnabled = false
            myCell.countryText.textColor = UIColor(red:0.17, green:0.67, blue:0.90, alpha:1.0)
            myCell.countryText.font = UIFont.boldSystemFont(ofSize: 16.0)
            myCell.daysLeft.textColor = UIColor(red:0.17, green:0.67, blue:0.90, alpha:1.0)
            myCell.daysLeft.font = UIFont.boldSystemFont(ofSize: 16.0)
            myCell.minutes.textColor = UIColor(red:0.17, green:0.67, blue:0.90, alpha:1.0)
            myCell.minutes.font = UIFont.boldSystemFont(ofSize: 16.0)
            
        }else{
            
            let currentCall = (indexPath as NSIndexPath).row - 1
            myCell.countryImage.layer.cornerRadius =  myCell.countryImage.frame.size.width / 2
            // myCell.countryImage.layer.borderColor = UIColor.whiteColor().CGColor
            //myCell.countryImage.layer.borderWidth = 1
            myCell.countryImage.clipsToBounds = true
            myCell.countryImage.image = UIImage(named: "\(pastIsoPrint[currentCall])")
            
            myCell.countryText.text = pastCountryPrint[currentCall]
            
            myCell.daysLeft.text = pastDays[currentCall]
            
            myCell.minutes.text = pastMinutes[currentCall]
            
            // myCell.countryText.text = "\(currentCall)"
        }
        return myCell
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //reload table when the user come back
        tableView.reloadData()
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
