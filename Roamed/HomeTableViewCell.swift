//
//  HomeTableViewCell.swift
//  Roamed
//
//  Created by QL on 21/7/16.
//  Copyright © 2016 SIMPSY LLP. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

  
    @IBOutlet weak var daysLeft: UILabel!
    @IBOutlet weak var countryText: UILabel!
    @IBOutlet weak var minutes: UILabel!
    @IBOutlet weak var countryImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
